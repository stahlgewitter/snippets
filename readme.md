---
title: Tetsu Snippet Manager
lang: en
---

# Characteristics
- offline & private
    - data stored in local sqlite database file
        - sync/backup yourself with whatever you want, it's just a file
    - no telemetry
- lightweight
    - ~ 15 MB disk space (contains backend and frontend)
    - low memory/cpu usage
    - the frontend runs in your installed browser

# Features
- instant search
- keyboard accessible with configurable shortcuts
- desktop integration: script to toggle frontend & paste to last active window (set global system shortcut)
    - currently only for Linux with X / X11 / Xorg (not Wayland)
- shift-paste support for pasting into Terminals
- syntax highlighting

# Screenshots
## Search
![Search](website/img/snippets_search_short.webp "Search")

## Create Snippet
![Create](website/img/snippets_create.webp "Create Snippet")

## Shortcuts
![Shortcuts](website/img/snippets_shortcuts.webp "Shortcuts")

# Video
[Watch (~750KB)](website/video/snippetdemo.mp4)


# Installation Linux
- [download release file](https://snippets.tetsu.eu/#download) & extract or build yourself (safer)
- run install.py in a terminal (inspect before) or run snippets binary manually
- set custom system shortcut to execute the toggle.py script (X/X11 systems) which shows/hides the UI
    - [KDE HowTo](https://userbase.kde.org/Tutorials/hotkeys)
    - [Gnome HowTo](https://help.gnome.org/users/gnome-help/stable/keyboard-shortcuts-set.html.en)
    - [Ubuntu HowTo](https://help.ubuntu.com/stable/ubuntu-help/keyboard-shortcuts-set.html.en)

# Build
- install dependencies
  - go
    - go get github.com/labstack/echo/v4
    - go get github.com/google/uuid
    - go get github.com/mattn/go-sqlite3
  - npm
- execute ./build.sh
- see created build dir in 'builds'

# Known Issues
- Firefox: needs about:config setting dom.events.testing.asyncClipboard set to true for copying to clipboard work

# Troubleshooting
- some browser extensions may interfere with shortcuts
- some shortcuts can't be configured because the browser prevents it (e.g. ctrl + n in chrome)

# Dev Notes
 
## Dependencies for the toggle & paste script 
- python3 (untested with Python 2)
- wmctrl
- xdotool
- xclip

# Limitations of Liability
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Used Open Source Libraries and their Licenses
- Bootstrap 5: MIT, [https://getbootstrap.com](https://getbootstrap.com), [License](https://github.com/twbs/bootstrap/blob/main/LICENSE)
- ReactJS: MIT, [https://reactjs.org](https://reactjs.org), [License](https://github.com/facebook/react/blob/main/LICENSE)
- Echo: MIT, [https://echo.labstack.com](https://echo.labstack.com), [License](https://github.com/labstack/echo/blob/master/LICENSE)
- HighlightJS: BSD 3-Clause License, [https://highlightjs.org](https://highlightjs.org), [License](https://github.com/highlightjs/highlight.js/blob/main/LICENSE)