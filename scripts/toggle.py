#!/usr/bin/env python3

# dependencies: 
# iproute (for ss, should be installed)
# awk (should be installed)
# wmctrl
# xdotool
# xclip

# to install for Debian, Ubuntu
# sudo apt install iproute xdotool wmctrl xclip

# to install for Fedora, Red Hat, CentOS
# sudo dnf install iproute xdotool wmctrl xclip

import webbrowser
import os
import re
import sys
import subprocess
import argparse

windowTitleSearchStr = 'Snippets 📝'
defaultPort = '55555'

try:
    stream = os.popen("ss -pltn | grep '\"snippets\",pid=' | awk '{print $4}' | awk -F':' '{print $2}'")
    port = stream.read().strip()

    if port and port.isnumeric():
        print('found port to be ' + port)
    else:
        port = defaultPort
        print('using default port: ' + port)

    launchUrl = 'http://localhost:' + port

    print(f'toggle: searching for window with title="{windowTitleSearchStr}"')
    regexEscapedWindowTitleSearchStr = re.escape(windowTitleSearchStr)

    isActive = False
    windowId = None

    # find active window
    activeWindowIdDecimalStr = subprocess.check_output("xdotool getwindowfocus", shell=True, universal_newlines=True).strip() # reports as decimal
    print('activeWindowIdDecimalStr ' + activeWindowIdDecimalStr)

    activeWindowTitle = subprocess.check_output("xdotool getwindowname " + activeWindowIdDecimalStr, shell=True, universal_newlines=True).strip()
    print('activeWindowTitle ' + activeWindowTitle)

    isActiveWindowSnippetApp = windowTitleSearchStr in activeWindowTitle
    if isActiveWindowSnippetApp:
        isActive = True
        windowId = int(activeWindowIdDecimalStr) if activeWindowIdDecimalStr != '' else -1
    else:
        # find any snippet window
        windowIdHexStr = subprocess.check_output("wmctrl -l | grep '" + regexEscapedWindowTitleSearchStr + "' | awk '{print $1}' | head -n 1", shell=True, universal_newlines=True)
        windowId = int(windowIdHexStr, 16) if windowIdHexStr != '' else -1

    print('windowId ' +  str(windowId))

    if windowId >= 0: # app already open
        if isActive: # is active -> hide
            print('minimize')
            subprocess.check_output('xdotool windowminimize ' + str(windowId), stderr=subprocess.STDOUT, shell=True, universal_newlines=True)

            # paste if arg to paste was given
            parser = argparse.ArgumentParser()
            parser.add_argument("-paste", nargs="?", const="no")
            args = parser.parse_args()
            if args.paste == 'ctrl_v':
                print('paste using ctrl + v')
                subprocess.check_output('xdotool key ctrl+v', stderr=subprocess.STDOUT, shell=True, universal_newlines=True)
            elif args.paste == 'ctrl_shift_v':
                print('paste using ctrl + shift + v')
                subprocess.check_output('xdotool key ctrl+shift+v', stderr=subprocess.STDOUT, shell=True, universal_newlines=True)
        
        else: # is not active -> activate
            print('activate')
            subprocess.check_output('xdotool windowactivate ' + str(windowId), stderr=subprocess.STDOUT, shell=True, universal_newlines=True)
    else:
        print('opening new')
        webbrowser.open_new(launchUrl)
except subprocess.CalledProcessError as e:
    # print output of failed command  
    print('CalledProcessError cmdOutput:\n' + str(e.output), file = sys.stderr)
    raise