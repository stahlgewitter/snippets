#!/usr/bin/env python3

import os
import pathlib
import subprocess
import webbrowser
import shutil

svcFileName = 'snippets.service'
defaultPort = '55555'

terminalColumnCount = shutil.get_terminal_size((80, 20))[0]

def YesNoInput(inputQuestion, default = 'y'):
    confirm = input(inputQuestion).strip()
    if (not confirm):
        confirm = default
    return confirm.lower().startswith('y')

def askForPort():
    while True:
        portInput = input(f'Set service port [{defaultPort}]: ')
        if (portInput == ''):
            return defaultPort
        if (portInput.isdecimal() and int(portInput) > 0  and int(portInput) < 65535):
            return portInput
        else:
            # ask again
            print('Port entered is invalid')

def printSeperator():
    print('#' * terminalColumnCount)

def installService():
    portInput = askForPort()
    workingDir = pathlib.Path().resolve()
    userSystemdPath = os.path.expanduser('~/.config/systemd/user/')
    targetFilePath = userSystemdPath + svcFileName
    serviceFileContent = f'''[Unit]
Description=Tetsu Snippet Manager
After=network.target

[Service]
WorkingDirectory={workingDir}
ExecStart={workingDir}/snippets -port {portInput}
Restart=always
RestartSec=5

[Install]
WantedBy=default.target
'''

    confirm = 'Preparing writing systemd user service file "' + targetFilePath + '" with content:\n================================\n' + serviceFileContent + '================================\n' + 'Do you confirm? (y/n) [y]: '
    if (YesNoInput(confirm)):
        
        # check if already exists
        svcFileExisted = os.path.exists(targetFilePath)
        if (svcFileExisted):
            if (not YesNoInput('A user service file with that name already exists. Overwrite? (y/n) [y]: ')):
                return

        with open(targetFilePath, 'w') as f:
            f.writelines(serviceFileContent)

        print(f'Written service file {svcFileName}.\nYou can use the systemctl commands to control it, such as:\n' 
        +f'\tsystemctl --user start {svcFileName}\n'
        +f'\tsystemctl --user stop {svcFileName}\n'
        +f'\tsystemctl --user enable {svcFileName}\n'
        +f'\tsystemctl --user disable {svcFileName}\n'
        )

        if (svcFileExisted):
            print('Reloading systemctl daemon')
            subprocess.check_output('systemctl --user daemon-reload', shell=True, text=True)
        
        print('Enabling service')
        subprocess.check_output('systemctl --user enable snippets', shell=True, text=True)

        print('Starting service')
        subprocess.run('systemctl --user restart snippets', shell=True, text=True)

        print('Service status output:')
        print(subprocess.run('systemctl --user --no-pager status snippets', shell=True, text=True))

        return { 'port': portInput }

# START LOGIC
port = None
if YesNoInput('Do you wish to install as a systemd user service? (y/n) [y]: '):
    result = installService()
    port = result['port']
    if (not port):
        port = defaultPort
printSeperator()

# check for x11 or wayland
print("The toggle script is only compatible with x11. Checking for current user...")
output = subprocess.check_output("loginctl show-session $(loginctl | grep $(whoami) | awk '{print $1}') -p Type | awk -F= '{print $2}'", shell=True, text=True).strip()
if (output == 'x11'):
    print('x11 was found, the toggle script is compatible with that.')
elif (output == 'wayland'):
    print("wayland was found, the toggle script currently only works for x11.")
else:
    print("could not determine, output was: " + output + ". The toggle script is compatible with x11.")

print()

# show instructions to install toggle script dependencies
print('The toggle and paste script needs some dependencies installed:')
print('To install on DEBIAN-based systems (such as Ubuntu), execute the following in a terminal:')
print('sudo apt install iproute xdotool wmctrl xclip')
print()
print('To install on REDHAT-based systems (such as Fedora, CentOS), execute the following in a terminal:')
print('sudo dnf install iproute xdotool wmctrl xclip')
print()
YesNoInput("I understand. [Enter]:")
printSeperator()

# give info for global shortcut
toggleScriptFile = os.path.dirname(os.path.abspath(__file__)) + "/toggle.py"
print('If you want to use the toggle script, setup a global system shortcut to execute: "' + toggleScriptFile + '"')
printSeperator()

appUrl = 'http://localhost:' + port
if (YesNoInput(f'Open App in Browser? [(]{appUrl}] (y/n) [y]: ')):
    webbrowser.open_new(appUrl)
printSeperator()

print()
print("All Done :)")