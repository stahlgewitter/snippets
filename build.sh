#!/usr/bin/env bash

time (
TIMEFORMAT="took %1Rs"
scriptDir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
baseDir=$scriptDir
backendDir="${baseDir}/backend/src"
buildDate="$(date +%y-%m-%dT%H%M%S)"
buildDir="${baseDir}/builds/${buildDate}"
buildZip="${buildDir}.zip"

cd $baseDir &&

echo 'building frontend...' &&
time (
    npm install --prefix react-frontend/ && 
    npm run build --prefix react-frontend/ 
) && echo &&

echo 'building backend...' &&
time (
    cd $backendDir && 
    find frontend/* ! -name DUMMYFILE -delete && # Delete frontend copy in backend
    cp -r ../../react-frontend/build/* ./frontend
    go build && 
    chmod g-w snippets && 
    echo "sha512sum: $(sha512sum snippets | awk '{print $1}')"
) && echo &&

echo "copying to build dir $buildDir" &&
time (
    mkdir -p $buildDir && 
    cp $backendDir/snippets $buildDir && 
    cp $baseDir/eula_oss.txt $buildDir && 
    cp -r $baseDir/scripts/* $buildDir
) && echo &&

echo "zipping build dir to: ${buildZip}" &&
time (
    cd $buildDir/.. && 
    ln -f -s ${buildDate} snippets && # make soft link and use that for zip, so dir in zip has correct name
    zip -r -9 -X ${buildDate}.zip snippets &&
    rm snippets && # delete soft link
    ls -lh ${buildZip} | awk '{print $5}' # print size of zip
) && echo &&

echo 'Finished successfully.'
)
