import React, { useEffect, useRef } from 'react';
import { Modal as BModal} from 'bootstrap'

const DEBUG = false

// wrapping bootstrap Modal
export default function Modal({isOpen, onIsOpenChanged, title, children}) {

    if (DEBUG)
        console.log("Modal:isOpen " + isOpen);

    const modalRef = useRef()

    // on isOpen changed
    useEffect(() => {
        function onHidden(e) {
            if (DEBUG)
                console.log('Modal:onHidden ' + JSON.stringify(e));

            let futureIsOpen = isOpen
            // if prop is still set to isOpen when hidden event comes in, 
            // the modal was closed by the modal itself: by clicking close button or outside the modal
            if (isOpen) {
                onIsOpenChanged(false)
                futureIsOpen = false
            }

            // fix to allow toggling while fade animation is running, see: https://github.com/twbs/bootstrap/issues/3902
            // if we're getting hidden event but modal is supposed to be shown -> show it
            if (futureIsOpen) {
                BModal.getOrCreateInstance(modalElem)?.show()
            }
        }
        
        function onShow(e) {
            if (DEBUG)
                console.log('Modal:onShow');
            
            onIsOpenChanged(true)
        }
        
        function onHide(e) {
            if (DEBUG)
                console.log('Modal:onHide ' + JSON.stringify(e));
        }

        function onShown(e) {
            if (DEBUG)
                console.log('Modal:onShown');
            
            // fix to allow toggling while fade animation is running, see: https://github.com/twbs/bootstrap/issues/3902
            // if we're getting shown event but modal is supposed to be closed -> close it
            if (!isOpen) {
                BModal.getInstance(modalElem)?.hide()
            }

            // autofocus elem to focus, see https://getbootstrap.com/docs/5.1/components/modal/#how-it-works
            if (isOpen) {
                // console.log("modalRef?.current?.querySelector('[autofocus]')" + modalRef?.current?.querySelector('[data-autofocus]'));
                modalRef?.current?.querySelector('[data-autofocus]')?.focus()
            }
        }

        const modalElem = modalRef?.current

        if (modalElem) {
            modalElem.addEventListener('show.bs.modal', onShow)
            modalElem.addEventListener('hidden.bs.modal', onHidden)

            modalElem.addEventListener('shown.bs.modal', onShown)
            modalElem.addEventListener('hide.bs.modal', onHide)

            if (isOpen) {
                if (DEBUG)
                    console.log('Modal:show()');

                BModal.getOrCreateInstance(modalElem).show();
            } else {
                if (DEBUG)
                    console.log('Modal:hide()');

                BModal.getInstance(modalElem)?.hide()
            }
        }
        return () => {
            if (modalElem) {
                if (DEBUG)
                    console.log('Modal: remove event listeners');
                modalElem.removeEventListener('show.bs.modal', onShow)
                modalElem.removeEventListener('hidden.bs.modal', onHidden)

                modalElem.removeEventListener('shown.bs.modal', onShown)
                modalElem.removeEventListener('hide.bs.modal', onHide)
            }
        }
    }, [isOpen, onIsOpenChanged])

    const wasNeverOpen = !isOpen && !modalRef?.current
    if (wasNeverOpen) // don't render anything if never opened
        return null;

    return (
      <div className="modal" tabIndex="-1" aria-labelledby="modalLabel" aria-hidden="true" ref={modalRef} >
        <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="modalLabel">{title}</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              {children}
            </div>
          </div>
        </div>
      </div>
    )

  }