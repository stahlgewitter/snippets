import React, { useState, useEffect, useCallback, useRef } from 'react';
import './App.css';
import Modal from './Modal'
import Toast from './Toast';
import Help from './Help';
import SnippetForm from './SnippetForm';
import hljs from "highlight.js";
import 'highlight.js/styles/a11y-light.css'
import { HIGHLIGHT_LANGUAGES_LOWERED } from "./Languages";
import { CommandType, commands } from './Commands';
import { searchSnippets, deleteSnippetById } from './api/snippets'
import { loadSettings, saveSettings, DEFAULT_SETTINGS } from './api/userSettings';
import { shortcutFromKeyEvent } from './utils';

const MIN_SEARCH_LEN = 2
const nameRegexForSnippetsToNotShorten = new RegExp('(cheat sheet)|(cheatsheet)', 'i') // prevent cheat sheet shortening (maybe put that in cfg)

function App() {
  const [searchQuery, setSearchQuery] = useState('');
  const [latestSearch, setLatestSearch] = useState();
  const [focusedSearchResult, setFocusedSearchResult] = useState();
  const [searchResultItems, setSearchResultItems] = useState(); // TODO maybe this should't be a state
  const [isHelpVisible, setIsHelpVisible] = useState(false)
  const [isCreateSnippetFormVisible, setIsCreateSnippetFormVisible] = useState(false)
  const [editSnippetFormState, setEditSnippetFormState] = useState({isVisible: false, snippet: undefined})
  const [settings, setSettings] = useState(DEFAULT_SETTINGS)
  const [commandsState, setCommandsState] = useState(commands)
  const [error, setError] = useState() //shown in modal
  const [latestMessageForUser, setLatestMessageForUser] = useState(undefined) // shown in toast
  const [isToastOpen, setIsToastOpen] = useState(false)
  const searchInput = useRef()

  // load user settings
  useEffect(() => {
    loadSettings().then(s => { 
      if (s)
        setSettings(prev => ({...prev, ...s}))
     } ).catch(e => {
       console.error(e)
       setLatestMessageForUser({title: 'Error loading user settings', message: e.message})
     })
  }, [])

  const isAnyModalOpen = useCallback(() => {
    return isHelpVisible || isCreateSnippetFormVisible || editSnippetFormState?.isVisible === true || error
  },[isHelpVisible, isCreateSnippetFormVisible, editSnippetFormState, error])

  // command functionality definitions
  const editFocusedCallback = useCallback((e) => {
    if (focusedSearchResult || editSnippetFormState.isVisible) {
      e.preventDefault()
      setEditSnippetFormState(prev => ({...prev, isVisible: !prev.isVisible, snippet: focusedSearchResult}))
    }
  }, [focusedSearchResult, editSnippetFormState])

  const focusSearchCallback = useCallback(e => {
    if (!isAnyModalOpen()) {
      e.preventDefault()
      searchInput?.current?.focus()
    }
  }, [isAnyModalOpen])

  const deleteFocusedCallback = useCallback(e => {
    if (!isAnyModalOpen()) {
      if (focusedSearchResult) {
        e.preventDefault()
        deleteSnippetFromSearch(focusedSearchResult)
      }
    }
  }, [isAnyModalOpen, focusedSearchResult])

  const focusNextSnippetCallback = useCallback(e => {
    if (!isAnyModalOpen()) {
      const searchResults = latestSearch?.searchResults
      if (searchResults?.length > 0) {
        e.preventDefault()
        
        const isDirectionDown = true;
        setFocusedSearchResult(getNextSearchResult(searchResults, focusedSearchResult, isDirectionDown))
      }
    }
  }, [isAnyModalOpen, latestSearch, focusedSearchResult])

  const focusPreviousSnippetCallback = useCallback(e => {
    if (!isAnyModalOpen()) {
      const searchResults = latestSearch?.searchResults
      if (searchResults?.length > 0) {
        e.preventDefault()
        
        const isDirectionDown = false;
        setFocusedSearchResult(getNextSearchResult(searchResults, focusedSearchResult, isDirectionDown))
      }
  }
}, [isAnyModalOpen, latestSearch, focusedSearchResult])

const selectSnippetCallback = useCallback(e => {
  if (isAnyModalOpen())
    return

    const target = e?.target;
    // Abort select command handling if target is a button/link. 
    // Allow Enter key on other valid elements such as buttons links to have precedence.
    if (target && (target instanceof HTMLButtonElement || target instanceof HTMLAnchorElement)) {
      return;
    }

    if (focusedSearchResult) {
      e.preventDefault()
      console.log('selected snippet ' + focusedSearchResult.name + ', copying value to clipboard');
      writeTextToClipboard(focusedSearchResult.value)
        .then(() => requestHideUI({pasteMethod: pasteMethodFromEvent(e)}))
        .catch(err => { console.log(err); setLatestMessageForUser({title: 'Copy to Clipboard Failure', message: 'error copying snippet to clipboard: ' + err?.message}) })
    } else {
      console.log('nothing focused to confirm');
    }
}, [focusedSearchResult, isAnyModalOpen])

const focusedSnippetToClipboardCallback = useCallback(e => {
  if (isAnyModalOpen())
    return

  async function writeTextToKeyboardWithMessage(text) {
    try {
      await writeTextToClipboard(text)
      setLatestMessageForUser({title: 'Copy to Clipboard Success', message: 'copied snippet to clipboard'})
    } catch (err) {
      setLatestMessageForUser({title: 'Copy to Clipboard Failure', message: 'error copying snippet to clipboard: ' + err?.message})
      // swallow err
    }
  }

    if (focusedSearchResult) {
      console.log('ctrl c detected, copying active');
      e.preventDefault()
      writeTextToKeyboardWithMessage(focusedSearchResult.value)
    }
}, [focusedSearchResult, isAnyModalOpen])

  // TODO Think about completely building commands here, then we can assemble everything together >> does the result have to go into a useState?
  // on init: connect commands to shortcut funcs/callbacks
  useEffect(() => {
    setCommandsState(prev => {
      const copy = new Map(prev)
      copy.get(CommandType.FocusSearch).commandFunc = focusSearchCallback
      copy.get(CommandType.EditFocused).commandFunc = editFocusedCallback
      copy.get(CommandType.DeleteFocused).commandFunc = deleteFocusedCallback
      copy.get(CommandType.FocusNextSnippet).commandFunc = focusNextSnippetCallback
      copy.get(CommandType.FocusPreviousSnippet).commandFunc = focusPreviousSnippetCallback
      copy.get(CommandType.SelectSnippet).commandFunc = selectSnippetCallback
      copy.get(CommandType.FocusedSnippetToClipboard).commandFunc = focusedSnippetToClipboardCallback
      copy.get(CommandType.ToggleHelp).commandFunc = e => {
        e.preventDefault()
        setIsHelpVisible(prev => !prev)
      }
       copy.get(CommandType.CreateSnippet).commandFunc = e => {
        e.preventDefault()
        setIsCreateSnippetFormVisible(prev => !prev)
      }
      return copy
    })
  }, [
    editFocusedCallback, 
    focusSearchCallback, 
    deleteFocusedCallback, 
    focusNextSnippetCallback, 
    focusPreviousSnippetCallback, 
    selectSnippetCallback,
    focusedSnippetToClipboardCallback])

  useEffect(() => {
    console.log('set commandsState');
      setCommandsState(prev => {
        const m = new Map()
        Array.from(prev.values()).forEach(c => {
          const userShortcut = settings.shortcuts.get(c.commandType)
          const newC = {...c}
          if (userShortcut) {
            console.log('setting user shortcut' + JSON.stringify(userShortcut))
            newC.shortcut = userShortcut
          }
          if (!newC.shortcut)
          newC.shortcut = newC.defaultShortcut
          m.set(newC.commandType, newC)
        })
        return m
      })
  }, [settings.shortcuts])

  const memoizedHandleKeyDown = useCallback(e => {
    // console.log('memoizedHandleKeyDown e.key ' + e.key);
    const shortcut = shortcutFromKeyEvent(e)
    const foundCmd = Array.from(commandsState.values()).find(c => areShortcutsEqual(c.shortcut, shortcut))
    if (foundCmd) {
      // console.log('foundCmd.commandFunc ' + JSON.stringify(foundCmd) + ', ' + foundCmd.commandFunc);
      if (foundCmd.commandFunc)
        foundCmd.commandFunc(e)
      else
        console.error('commandFunc falsey for shortcut ' + JSON.stringify(shortcut));
    } else {
      // console.log('no cmd found');
    }
  }, [commandsState]);
  
  // setup global keyboard handler
  useEffect(() => {
    document.addEventListener('keydown', memoizedHandleKeyDown)
    return () => {
      document.removeEventListener('keydown', memoizedHandleKeyDown)
    }
  }, [memoizedHandleKeyDown])

  // trigger search
  useEffect(() => {
    async function onSearchQueryChanged() {
      if (!searchQuery || searchQuery.length < MIN_SEARCH_LEN) {
        // console.log('setting latest search to undefined because query is falsey or searchQuery length < MIN_SEARCH_LEN');
        setLatestSearch(undefined)
        return
      }

      try {
        const snippetSearch = await searchSnippets(searchQuery)
        if (snippetSearch.searchQuery === searchQuery) {
          // console.log("setting latest search for searchQuery:" + searchQuery);
          // console.log(JSON.stringify(snippetSearch));
          setLatestSearch(snippetSearch)
        } else {
          console.log('discarding search result ' + snippetSearch.searchQuery + ", because current searchQuery is " + searchQuery);
        }
      } catch (error) {
        console.error(error);
        setLatestMessageForUser({title: 'Search Error', message: error.message})
      }
    }

    onSearchQueryChanged().catch(console.error)
  }, [searchQuery])

  // auto focus first result after search results come back 
  useEffect(() => {
    if (latestSearch) {
      const isAnySearchResultFocused = latestSearch?.searchResults?.indexOf(focusedSearchResult) >= 0
      if (!isAnySearchResultFocused) {
        setFocusedSearchResult(getNextSearchResult(latestSearch.searchResults, focusedSearchResult, true))
      }
    }
  }, [latestSearch, focusedSearchResult])

  // on latestSearch change: clear focused search result if there are no results
  useEffect(() => {
  const searchResultCount = latestSearch?.searchResults?.length ?? 0
  if (searchResultCount === 0) {
    setFocusedSearchResult(undefined)
  }
}, [latestSearch])

// open toast on new message for user
useEffect(() => {
  setIsToastOpen(latestMessageForUser ? true : false)
}, [latestMessageForUser])

  // render search result html
  useEffect(() => {
    // console.log('rendering search result');
    if (!latestSearch) {
      setSearchResultItems(undefined)
      return
    }

    // set valueToDisplay property, possibly truncating snippet
    const maxSnippetLinesInSearchResult = settings.maxSnippetLinesInSearchResult
    latestSearch?.searchResults?.forEach(sr => {
        sr.valueToDisplay = sr.value // default everything, may be overridden later
        if (maxSnippetLinesInSearchResult >= 0 && sr.isDisplayValueShortened !== false) {
          if (!nameRegexForSnippetsToNotShorten.test(sr?.name)) {
            const split = sr.value?.split(/\n/)
            if (split && split.length > maxSnippetLinesInSearchResult) {
              sr.valueToDisplay = split.slice(0, maxSnippetLinesInSearchResult).join('\n')
              sr.isDisplayValueShortened = true
            }
          }
        }
      })

    // highlight valueToDisplay
    if (settings.useSyntaxHighlighting) {
      latestSearch?.searchResults?.forEach(sr => {
        const hasTruncationChanged = sr.valueToDisplay.length !== sr.highlightedSourceLength 
        if (!sr.highlighted || hasTruncationChanged) { // only if not already highlighted
          if (sr.language && HIGHLIGHT_LANGUAGES_LOWERED.includes(sr.language.toLowerCase())) {
            sr.highlighted = hljs.highlight(sr.valueToDisplay, {language: sr.language, ignoreIllegals: true})
          } else {
            sr.highlighted = hljs.highlightAuto(sr.valueToDisplay)
          }
          sr.highlightedSourceLength = sr.valueToDisplay.length
        }
      })
    }
  
    // render items
    const searchResultItems = latestSearch?.searchResults?.map((r, i) => (
      <li className={`search-result list-group-item${r === focusedSearchResult ? ' active' : '' }`} key={i}
        aria-current={r === focusedSearchResult} 
        data-id={r.id}
        title={r.isDisplayValueShortened ? r.value : null}
        onClick={e => {
          console.log('clicked snippet=' + r.name);
          setFocusedSearchResult(r)
          
          // not using focusedSearchResult here, hasn't changed yet
          // for the same reason not calling selectSnippetCallback
          writeTextToClipboard(r.value)
            .then(() => requestHideUI({pasteMethod: pasteMethodFromEvent(e)}))
            .catch(err => { console.log(err); setLatestMessageForUser({title: 'Copy to Clipboard Failure', message: 'error copying snippet to clipboard: ' + err?.message}) })
        }}
      >
        <h6 className="d-flex align-items-baseline mb-0">
            <span>{r.name}</span>
            {r.language &&
            <>
            <span>&nbsp;|&nbsp;</span>
            <span className="Language">{r.language}</span>
            </>
          }

            <button className="btn btn-link py-0" style={{fontSize: "0.85rem"}} title="" data-edit
              onClick={e => { 
                e.preventDefault(); 
                e.stopPropagation();
                setEditSnippetFormState({isVisible: true, snippet: r}) 
              }}>
              edit
            </button>

            <button className="btn btn-link text-danger ms-auto py-0" title="Delete" data-delete
              onClick={e => { e.preventDefault(); e.stopPropagation(); deleteSnippetFromSearch(r); } }>
                x
            </button>
        </h6>
        <div className="text-muted" style={{fontSize: "0.75rem"}}>{r.description}</div>
       {( settings.useSyntaxHighlighting && r.highlighted
       ? <div><pre className="mb-0"><code dangerouslySetInnerHTML={{ __html: r?.highlighted?.value }}></code></pre></div>
        : <div><pre className="mb-0"><code>{r.value}</code></pre></div>
       )}

       { r.isDisplayValueShortened === true &&
          // render expand button 
          <div className="d-flex flex-wrap align-items-center" style={{fontSize: "0.75rem"}}>
            <span>&hellip;&nbsp;truncated,&nbsp;</span>
            <button className="btn btn-link ps-0" style={{fontSize: "0.75rem"}} title="show full snippet value"
              onClick={e => {
                e.preventDefault()
                e.stopPropagation() // so global shortcut handler isn't triggered

                setLatestSearch(prev => {
                  let srMod = prev.searchResults.map(sr => {
                    if (sr === r) {
                      sr.isDisplayValueShortened = false
                    }
                    return sr
                  })
                  return {...prev, searchResults: srMod}
                })
                }}>
                expand
            </button>
          </div>
       }

      </li>
    ));

    setSearchResultItems(searchResultItems)
  }, [latestSearch, focusedSearchResult, settings.maxSnippetLinesInSearchResult, settings.useSyntaxHighlighting])

  // keep focused in view: scroll (if necessary)   
  useEffect(() => {
    if (focusedSearchResult) {
      const elem = document.querySelector('.search-result[data-id="' + focusedSearchResult.id + '"]')
      if (elem) {
        const alignToTop = true
        elem.scrollIntoView(alignToTop)
      }
    }
  }, [focusedSearchResult])

  // focus searchInput after all modals have closed
  useEffect(() => {
    if (!isAnyModalOpen()) {
      // called once at init unnecesarily, but doesn't matter since it's focused anyways
      searchInput?.current?.focus()
    }
  }, [isAnyModalOpen])

    let app = (
    <div className="App">
      <div className="d-flex sticky-top" style={{paddingTop: '8px', background: 'var(--bs-body-bg)'}}>
        <button onClick={() => setIsCreateSnippetFormVisible(prev => !prev)} title="create new snippet" className="btn btn-secondary create-snippet-btn">
          +
        </button>
        <input type="search" placeholder="search" autoFocus className="form-control search-input" ref={searchInput}
          value={searchQuery} onInput={(e) => setSearchQuery(e.target.value) }></input>
      </div>

        { !searchQuery &&
          <button className="btn btn-secondary mt-2" onClick={e => setIsHelpVisible(prev => !prev)}>Help</button>
        }

        <ul className="search-results list-group">
          {searchResultItems}
        </ul>

      {isToastOpen && latestMessageForUser &&
      <div className="position-fixed bottom-0 end-0 p-3" style={{zIndex:3}}>
        <Toast 
          isOpen={isToastOpen} 
          onIsOpenChanged={isOpen => setIsToastOpen(isOpen)}
          title={latestMessageForUser.title}>
          {latestMessageForUser.message}
        </Toast>
      </div>
      }

      <div className="SnippetFormModalParent">
        <Modal title="Create Snippet" 
          isOpen={isCreateSnippetFormVisible} 
          onIsOpenChanged={isOpen => setIsCreateSnippetFormVisible(isOpen)}>
          <SnippetForm 
            onSubmitSuccess={(formData) => setIsCreateSnippetFormVisible(false)} 
            settings={settings}/>
        </Modal>
      </div>

      <div className="SnippetFormModalParent">
        <Modal title="Edit Snippet" 
          isOpen={editSnippetFormState?.isVisible} 
          onIsOpenChanged={isOpen => setEditSnippetFormState(prev => ({...prev, isVisible: isOpen}))}>
          <SnippetForm 
            snippet={editSnippetFormState?.snippet}
            settings={settings}
            onSubmitSuccess={(formData) => { 
              setEditSnippetFormState(prev => ({isVisible: false, snippet: undefined}))
              if (formData && formData.id) {
                // update changed snippet in search results to reflect changed
                setLatestSearch(prev => 
                  ({...prev, 
                    searchResults: 
                      prev.searchResults.map(x => 
                        x.id === formData.id 
                          ? {...x, ...formData}
                          : x)
                  }))
              }
              } } />
        </Modal>
      </div>

      {error &&
        <Modal title="Error" isOpen={error !== null && error !== undefined} 
            onIsOpenChanged={isOpen => { 
              if (!isOpen) // clear error on close
                setError(undefined) 
            }}>
          <div>action: {error?.action}</div>
          <div>message: {error?.error?.message}</div>
        </Modal>
      }

      <Modal title="Help" isOpen={isHelpVisible} onIsOpenChanged={isOpen => setIsHelpVisible(isOpen)}>
        <Help commandTypeToShortcutMap={settings.shortcuts} onCommandTypeToShortcutMapChanged={onCommandTypeToShortcutMapChanged}/>
      </Modal>

    </div>
    );
  

  return app;

  function onCommandTypeToShortcutMapChanged(changed) {
    console.log('onCommandTypeToShortcutMapChanged' + JSON.stringify([...changed.entries()]))
    const newSettings = {...settings, shortcuts: changed}
    console.log('newsett ' + JSON.stringify(newSettings));
    saveSettings(newSettings)
    setSettings(newSettings)
  }

  function deleteSnippetFromSearch(snippet) {
      deleteSnippetById(snippet.id)
      .then(() => {
        setLatestSearch(prev => ({...prev, searchResults: prev.searchResults.filter(r => r !== snippet)}))
      }).catch(err => setError({action: "deleting snippet " + snippet.id, error: err}))
  }

  function writeTextToClipboard(text) {
    return navigator.clipboard.writeText(text)
  }

}

// stateless functions (outside of the component)

// KeyEvent
function areShortcutsEqual(s1, s2) {
  if (s1.key !== s2.key)
    return false;

  if (s1.altKey === true && s2.altKey !== true)
    return false

  if (s1.ctrlKey === true && s2.ctrlKey !== true)
    return false

  if (s1.shiftKey === true && s2.ShiftKey !== true)
    return false

  if (s1.metaKey === true && s2.metaKey !== true)
    return false

  return true
}

// TODO move to data js file
async function requestHideUI({pasteMethod} = {}) {
  let url = "/api/hideUI"

  let queryParams = undefined
  if (pasteMethod) {
    queryParams = 'paste=' + encodeURIComponent(pasteMethod) 
  }

  if (queryParams) {
    url += '?' + queryParams
  }

  const res = await fetch(url, {method: 'POST'})
  console.log("hideUI response: " +res.status)
}

// taking currently focused, returning next to be focused when going down or up
function getNextSearchResult(searchResults, focusedSearchResult, isDirectionDown) {
  // console.log('focus next search result ' + (isDirectionDown ? 'down' : 'up'));
  const selectedIdx = searchResults.indexOf(focusedSearchResult);
  if (selectedIdx < 0) { // nothing selected yet
    const newSelectedIdx = isDirectionDown ? 0 : searchResults[searchResults.length - 1];
    return searchResults[newSelectedIdx];
  } else { // already has selection
    let newSelectedIdx = 0;
    if (isDirectionDown) {
      newSelectedIdx = selectedIdx + 1;
      if (newSelectedIdx >= searchResults.length) { // wrap forward over bottom to start
        newSelectedIdx = 0;
      }
    } else { // up
      newSelectedIdx = selectedIdx - 1;
      if (newSelectedIdx < 0) { // wrap backward over start to end
        newSelectedIdx = searchResults.length - 1;
      }
    }
    return searchResults[newSelectedIdx];
  }
}

function pasteMethodFromEvent(e) {
  return e?.shiftKey ? 'ctrl_shift_v' : 'ctrl_v'
}

export default App;
