export function shortcutFromKeyEvent(e) {
   const shortcut = {key: e.key}
    if (e.ctrlKey) {
      shortcut.ctrlKey = e.ctrlKey
    }
    if (e.altKey) {
      shortcut.altKey = e.altKey
    }
    if (e.shiftKey) {
      shortcut.shiftKey = e.shiftKey
    }
    if (e.metaKey) {
      shortcut.metaKey = e.metaKey
    }
    return shortcut
}