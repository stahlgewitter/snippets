import { useCallback, useEffect, useRef, useState } from 'react';
import { commands } from './Commands';
import { shortcutFromKeyEvent } from './utils';
import './Help.css';

const EditAction = {
  StartEditMode : 'StartEditMode',
  ConfirmEditMode: 'ConfirmEditMode',
  CancelEditMode :'CancelEditMode',
}

const emptyEditState = {editAction: undefined, commandType: undefined, shortcut: undefined} 

export default function Help({commandTypeToShortcutMap, onCommandTypeToShortcutMapChanged}) {
    const [editState, setEditState] = useState(emptyEditState)
    const helpContainer = useRef()

    const onKeyDown = useCallback(e => {

      // prevent browser functionality key codes to interfere when letting user set shortcut
      if (editState.editAction === EditAction.StartEditMode) {
        e.preventDefault();
        e.stopPropagation();
      }

      // console.log('OKD editState.editAction ' +editState.editAction + ", " + e.key);
      if (e.key === "Escape" && editState.editAction === EditAction.StartEditMode) {
        setEditState(prev => ({...prev, editAction: EditAction.CancelEditMode, startedWithDevice: undefined}))
      }
    }, [editState.editAction])

    const onKeyUp = useCallback(e => {
      // console.log('e.keyup in help ' + e.key + ' , editState.openMethod' + editState.editAction);
      // don't leak keyup events when in edit mode
      if (editState.editAction === EditAction.StartEditMode) {
        e.preventDefault();
        e.stopPropagation();
      }

      // disallow escape as shortcut
      if (e.key === 'Escape') {
        return
      }

      if (editState.startedWithDevice === 'keyboard' && e.key === "Enter") {
        // ignore Enter up event if editing was triggered by keyboard enter, because it's the Up event of user entering the edit mode
        // clear startedWIthDevice so the next Enter can be used as a shortcut
        setEditState(p => ({...p, startedWithDevice: undefined}))
      } else {
        const shortcut = shortcutFromKeyEvent(e)
        // determine if valid
        if (isShortcutValid(shortcut)) {
          setEditState(prev => ({
            ...prev, 
            editAction: EditAction.ConfirmEditMode, 
            shortcut : shortcutFromKeyEvent(e), 
            startedWithDevice: undefined
          }))
        } else {
          console.log('invalid shortcut');
        }
    }
    }, [editState])

    useEffect(() => {
      const currentHelpContainer = helpContainer.current

      switch (editState.editAction) {
        case EditAction.StartEditMode:
          console.log('start edit mode');
          // remove in case there was one registered before (highlander principle 'there can only be one')
          // this is the case when one shortcut is in edit mode and another is being started
          currentHelpContainer.removeEventListener('keyup', onKeyUp)
          currentHelpContainer.addEventListener('keyup', onKeyUp)

          currentHelpContainer.removeEventListener('keydown', onKeyDown)
          currentHelpContainer.addEventListener('keydown', onKeyDown)
          break;

        case EditAction.ConfirmEditMode:
          currentHelpContainer.removeEventListener('keydown', onKeyDown)
          currentHelpContainer.removeEventListener('keyup', onKeyUp)

          setEditState(emptyEditState)

          onCommandTypeToShortcutMapChanged(new Map([...commandTypeToShortcutMap, [editState.commandType, editState.shortcut]]))

          // TODO set shortcut, but not like this, command stays pure, call props fn
          // commands.get(editState.command).shortcut = editState.shortcut
          break;
        case EditAction.CancelEditMode:
        console.log('cancel');
        currentHelpContainer.removeEventListener('keydown', onKeyDown)
        currentHelpContainer.removeEventListener('keyup', onKeyUp)
        setEditState(emptyEditState)
        break;

      default:
        // console.log('unknown edit mode ' + editState.editAction);
        break;
      }

      return () => {
        currentHelpContainer.removeEventListener('keydown', onKeyDown)
        currentHelpContainer.removeEventListener('keyup', onKeyUp)
      }
    }, [editState, onKeyDown, onKeyUp, commandTypeToShortcutMap, onCommandTypeToShortcutMapChanged]);

    const commandsElems = Array.from(commands.values()).map(c => (
      <tr key={c.name}>
      <th scope="row">
          <button className="btn btn-secondary" title={editState.editAction !== EditAction.StartEditMode ? 'Click to change' : 'Press Shortcut'}
          onClick={e => setEditState(prev => { return ({...prev, editAction: EditAction.StartEditMode, commandType: c.commandType, startedWithDevice: (e.pageY === 0 ? 'keyboard' : 'mouse')}) }) }
            dangerouslySetInnerHTML={{__html: editState.editAction === EditAction.StartEditMode && editState.commandType === c.commandType ? '...' :  shortcutToHtml(getShortcut(c, commandTypeToShortcutMap)) }}>
          </button></th>
      <td>{c.name}</td>
    </tr>
    ));

    return (
        <table className="table shortcuts" ref={helpContainer}>
        <thead>
          <tr>
            <th scope="col">Shortcut</th>
            <th scope="col">Effect</th>
          </tr>
        </thead>
        <tbody>
          {commandsElems}
        </tbody>
      </table>
    )
}

function getShortcut(command, commandTypeToShortcutMap) {
  const s = commandTypeToShortcutMap.get(command.commandType)
  if (s)
    return s

  return command.defaultShortcut
}

function isShortcutValid(s) {
  const invalidShortcutKeys = ["Control", "Alt", "Meta", "Shift"] // they may be modifiers but not the key itself
  return !invalidShortcutKeys.includes(s.key)
}

function shortcutToHtml(s) {
  if (!s)
  return ''
  
  let parts = []
  if (s.ctrlKey) {
    parts.push('Ctrl')
  }
  if (s.altKey) {
    parts.push('&#8997;&nbsp;Alt')
  }
  if (s.key) {
    if (s.key === 'ArrowDown') {
      parts.push('&#8595;&nbsp;' + s.key)
    } else if (s.key === 'ArrowUp') {
      parts.push('&#8593;&nbsp;' + s.key)
    } else {
      parts.push(s.key)
    }
  }

  return parts.join('&nbsp;+&nbsp;')
}