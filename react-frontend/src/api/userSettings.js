export const DEFAULT_SETTINGS = {
    version: 1,
    useSyntaxHighlighting: true,
    maxSnippetLinesInSearchResult: 8,
    shortcuts: new Map(), // <CommandType, Shortcut> 
    allowTabInSnippetInput: true,
    defaultLanguage: ""
  }

export async function loadSettings() {
    let url = "/api/setting"
    let response = await fetch(url)

    // not yet created
    if (response.status === 404) {
        return undefined
    }

    if (response.status !== 200) {
      throw Error("bad response with status " + response.status)
    }

    const settingsJson = await response.json();

    // create shortcuts map from array (stored as array of kv-arrays [[k1, v1], [k2, v2]])
    if (settingsJson.shortcuts) {
        // console.log('settingsJson.shortcuts ' + JSON.stringify(settingsJson.shortcuts));
        try {
            settingsJson.shortcuts =  new Map(settingsJson.shortcuts)
        } catch(err) {
            console.log('error creating shortcuts map ' + err);
            settingsJson.shortcuts = new Map()
        }
        // console.log('settingsJson.shortcuts  ' + JSON.stringify(settingsJson.shortcuts.get('CreateSnippet')))
    } else {
        settingsJson.shortcuts = new Map()
    }

    return settingsJson
} 

export async function saveSettings(settings) {
    const bodyStr = JSON.stringify({...settings, shortcuts: [...settings.shortcuts]}) // converting shortcuts map to arr
    console.log('saving ' + bodyStr);
    let url = "/api/setting"
    const res = await fetch(url, {
        method: "POST", 
        body: bodyStr, 
        headers: {
            'Content-Type': 'application/json'
    }})
    if (res.status !== 200) {
      let bodyText = ''
      try {
        bodyText = await res.text()
      } catch (err) {
        console.log('error getting body text: ' + err);
      }
      throw new Error(`status: ${res.status}, body: ${bodyText}`)
    }
}