export async function searchSnippets(searchQuery) {
    let url = "/api/snippet?q=" + encodeURIComponent(searchQuery)
    let response = await fetch(url)
    if (response.status !== 200) {
      throw Error("bad response with status " + response.status)
    }
    const searchResultJson = await response.json();
    const search = {
      "searchQuery" : searchQuery,
      "searchResults" : searchResultJson
    }
    return search
  }
  
  export  async function deleteSnippetById(id) {
    let url = "/api/snippet/" + id
    const res = await fetch(url, {method: 'DELETE'})
    if (res.status !== 200) {
      let bodyText = ''
      try {
        bodyText = await res.text()
      } catch (err) {
        console.log('error getting body text: ' + err);
      }
      throw new Error(`status: ${res.status}, body: ${bodyText}`)
    }
  }