import { useState } from "react"
import { HIGHLIGHT_LANGUAGES, HIGHLIGHT_LANGUAGES_LOWERED } from "./Languages";

const FormType = {
    Create: 'Create',
    Update : 'Update'
}

const emptyFormData = {name: '', description: '', value: '', language: ''}

const languageOptions = HIGHLIGHT_LANGUAGES.map(l => {
    return (<option value={l} key={l}/>)
})

// create (snippet prop undefined) or edit snippet
export default function SnippetForm({snippet, onSubmitSuccess, settings}) {
    
    const [formData, setFormData] = useState(emptyFormData)
    const [errorMsg, setErrorMsg] = useState(undefined)
    
    const formType = snippet ? FormType.Update : FormType.Create
    
    if (formType === FormType.Create) {
        emptyFormData.language = settings.defaultLanguage
    }

  if (snippet && snippet.id !== formData?.id) {
      setFormData(({
        id : snippet.id,
        name: snippet.name, 
        description: snippet.description, 
        language: snippet.language, 
        value: snippet.value}))
  }

  return (
        <form style={{height: "100%", display: "grid", gridTemplateRows: "auto auto 1fr 4fr"}} onSubmit={onSubmit} onKeyDown={onKeyDown}>
            <div className="mb-3">
                <label htmlFor="nameInput" className="form-label">Name *</label>
                <input id="nameInput" type="text" className="form-control" required data-autofocus
                    value={formData.name} onChange={e => setFormData(prev => ({...prev, name: e.target.value}))}></input>
            </div>

            <div className="mb-3">
                <label htmlFor="languageInput" className="form-label">Language</label>
                <input className="form-control" list="datalistOptions" id="exampleDataList" placeholder={ settings.defaultLanguage ? settings.defaultLanguage : "" }
                    value={formData.language} onChange={e => setFormData(prev => ({...prev, language: e.target.value}))} />
                <datalist id="datalistOptions">
                    {languageOptions}
                </datalist>
            </div>

            <div className="mb-3">
                <label htmlFor="descriptionInput" className="form-label">Description</label>
                <textarea id="descriptionInput" type="text" className="form-control"
                    value={formData.description} onChange={e => setFormData(prev => ({...prev, description: e.target.value}))}></textarea>
            </div>

            <div className="mb-3" style={{display: "grid", gridTemplateRows: "auto 1fr"}}>
                <label htmlFor="snippetInput" className="form-label d-flex align-items-baseline">Snippet *{settings.allowTabInSnippetInput && <span style={{fontSize: '0.7rem', marginLeft: 'auto'}}> [Tabbing is enabled]</span>}</label>
                <textarea id="snippetInput" className="form-control" required
                    value={formData.value} onChange={e => setFormData(prev => ({...prev, value: e.target.value}))}
                    onKeyDown={e => {
                        if (settings.allowTabInSnippetInput) {
                            if (e.key === 'Tab' && !e.ctrlKey && !e.altKey && !e.metaKey) {
                                e.preventDefault()
                                e.stopPropagation()
                                // using deprecated (but still all supported) execCommand so undo history is kept (we can ctrl+z in the browser to undo)
                                if (e.shiftKey) { // shift tab
                                    var text = e.target.value;
                                    if (e.target.selectionStart > 0 && text[e.target.selectionStart-1] === '\t')
                                    {
                                        document.execCommand('delete');
                                    }
                                } else { // tab
                                    document.execCommand('insertText', false, "\t");
                                }
                            }
                        }
                    }}></textarea>
            </div>

            {errorMsg &&
            <div className="alert alert-danger mb-3" role="alert">
                {errorMsg}
            </div>
            }

            <div className="d-flex">
                <button className="btn btn-primary" style={{justifySelf: "flex-start"}} type="submit">
                    Save 
                    <span style={{fontSize: "0.6rem", verticalAlign: "middle"}}> (Ctrl + Enter)</span>
                </button>
            </div>
        </form>
    )

    async function onSubmit(e) {
        e.preventDefault();
        await submitForm()
    }

    async function submitForm() {
        let res = undefined

        // automatically convert user input language to correct casing if it is one of the predefined ones
        if (!HIGHLIGHT_LANGUAGES.includes(formData.language)) {
            const idx = HIGHLIGHT_LANGUAGES_LOWERED.indexOf(formData.language)
            if (idx >= 0) {
                formData.language = HIGHLIGHT_LANGUAGES[idx]
            }
        }

        switch(formType) {
            case FormType.Create:
                console.log('Create');
                const formDataWithId = {...formData, id: crypto.randomUUID()}
                const formDataWithIdStr = JSON.stringify(formDataWithId)
                // console.log('submit, data ' + formDataWithIdStr);
                res = await fetch('/api/snippet', {
                    method: "POST", 
                    body: formDataWithIdStr, 
                    headers: {
                        'Content-Type': 'application/json'
                }})
            break;
            case FormType.Update:
                const formDataStr = JSON.stringify(formData)
                res = await fetch('/api/snippet/' + snippet.id, {
                    method: "PUT", 
                    body: formDataStr, 
                    headers: {
                        'Content-Type': 'application/json'
                }})
            break;
            default:
                throw new Error('unknown form type ' + formType)
        }

        if (res.status === 200) {
            setErrorMsg(undefined)
            
            if (formType === FormType.Create)
                setFormData(emptyFormData)

            console.log('{...formData} ' + JSON.stringify({...formData}));

            if (onSubmitSuccess)
                onSubmitSuccess({...formData})
        } else {
            let bodyText = ''
            try {
                bodyText = await res.text()
            } catch (err) {
                console.log('error awaiting response text: ' + err)
            }
            const errorMsg = `error: status: ${res.status}, msg: ${bodyText}`
            console.log(errorMsg);
            setErrorMsg(errorMsg)
        }
    }

    function onKeyDown(e) {
        if (e.key === "Enter" && e.ctrlKey) {
            e.preventDefault()
            submitForm()
        }
    }
}