import React, { useEffect, useRef } from 'react';
import { Toast as BToast } from 'bootstrap';

const DEBUG = false

// not tested with data-bs-animation="false"
export default function Toast({isOpen, onIsOpenChanged, title, children}) {

    const rootRef = useRef()

    useEffect(() => {
        function onShow(e) {
            if (DEBUG)
                console.log('Toast:onShow');
            
            onIsOpenChanged(true)
        }

        function onHidden(e) {
            if (DEBUG)
                console.log('Toast:onHidden ' + JSON.stringify(e));
                
            onIsOpenChanged(false)
        }

        function onShown(e) {
            if (DEBUG)
                console.log('Toast:onShown');
        }

        function onHide(e) {
            if (DEBUG)
                console.log('Toast:onHide ' + JSON.stringify(e));
        }

        const modalElem = rootRef?.current
        if (modalElem) {
            modalElem.addEventListener('show.bs.toast', onShow)
            modalElem.addEventListener('shown.bs.toast', onShown)
            
            modalElem.addEventListener('hide.bs.toast', onHide)
            modalElem.addEventListener('hidden.bs.toast', onHidden)

            if (isOpen) {
                if (DEBUG)
                    console.log('Modal:show()');

                BToast.getOrCreateInstance(modalElem).show();
            } else {
                if (DEBUG)
                    console.log('Modal:hide()');

                BToast.getInstance(modalElem)?.hide()
            }
        }

        return () => {
            if (modalElem) {
                modalElem.removeEventListener('show.bs.toast', onShow)
                modalElem.removeEventListener('shown.bs.toast', onShown)
                
                modalElem.removeEventListener('hide.bs.toast', onHide)
                modalElem.removeEventListener('hidden.bs.toast', onHidden)
            }
        }
    }, [isOpen, onIsOpenChanged])

    return (
<div className="toast" role="alert" aria-live="assertive" aria-atomic="true" data-bs-animation="false" ref={rootRef}
    data-bs-delay="5000">
  <div className="toast-header" style={{alignItems: 'baseline'}}>
    {/* <img src="..." className="rounded me-2" alt="..."> */}
    <strong className="me-auto">{title}</strong>
    <small className="text-muted">{new Date().toLocaleTimeString()}</small>
    <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
  </div>
  <div className="toast-body">
    {children}
  </div>
</div>
    )
}