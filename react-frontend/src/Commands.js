export const CommandType = {
    ToggleHelp: 'ToggleHelp',
    CreateSnippet: 'CreateSnippet',
    EditFocused: 'EditFocused',
    DeleteFocused: 'DeleteFocused',
    FocusSearch: 'FocusSearch',
    FocusNextSnippet: 'FocusNextSnippet',
    FocusPreviousSnippet: 'FocusPreviousSnippet',
    SelectSnippet: 'SelectSnippet',
  }
  
  export const commands = new Map([
    [CommandType.ToggleHelp,{commandType: CommandType.ToggleHelp, name: 'Toggle Help', defaultShortcut: {key: 'F1'}}],
    [CommandType.CreateSnippet, {commandType: CommandType.CreateSnippet, name: 'Create new snippet', defaultShortcut: {altKey: true, key: 'n'}}],
    [CommandType.EditFocused, {commandType: CommandType.EditFocused, name: 'Edit focused snippet', defaultShortcut: {altKey: true, key: 'e'}}],
    [CommandType.DeleteFocused, {commandType: CommandType.DeleteFocused, name: 'Delete focused snippet', defaultShortcut: {altKey: true, key: 'Delete'}}],
    [CommandType.FocusSearch, {commandType: CommandType.FocusSearch, name: 'Focus search', defaultShortcut: {altKey: true, key: 'l'}}],
    [CommandType.FocusNextSnippet, {commandType: CommandType.FocusNextSnippet, name: 'Focus next snippet', defaultShortcut: {key: 'ArrowDown'}}],
    [CommandType.FocusPreviousSnippet, {commandType: CommandType.FocusPreviousSnippet, name: 'Focus previous snippet', defaultShortcut: {key: 'ArrowUp'}}],
    [CommandType.SelectSnippet, {commandType: CommandType.SelectSnippet, name: 'Select focused snippet', defaultShortcut: {key: 'Enter'}}],
    [CommandType.FocusedSnippetToClipboard, {commandType: CommandType.FocusedSnippetToClipboard, name: 'Copy focused snippet to clipboard', defaultShortcut: {ctrlKey: true, key: 'c'}}],
  ])
  