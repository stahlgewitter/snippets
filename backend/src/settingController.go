package main

import (
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
)

const settingsFile = "settings.json"

func readSettings(c echo.Context) error {
	file, err := os.Open(settingsFile)
	if errors.Is(err, os.ErrNotExist) {
		return c.NoContent(http.StatusNotFound)
	} else {
		file.Close()
	}

	bytes, err := ioutil.ReadFile(settingsFile)
	if err != nil {
		return err
	}

	return c.JSONBlob(http.StatusOK, bytes)
}

func writeSettings(c echo.Context) error {
	bodyBytes, err := io.ReadAll(c.Request().Body)
	if err != nil {
		return err
	}

	file, err := os.Create(settingsFile)
	if err != nil {
		return err
	}

	n, err := file.Write(bodyBytes)
	if err != nil {
		return err
	}

	log.Printf("written %v bytes", n)

	return c.NoContent(http.StatusOK)
}
