package main

import (
	"database/sql"
	"embed"
	_ "embed"
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
	"os/exec"
	"time"

	"github.com/google/uuid"
	// MIT licensed
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"golang.org/x/net/http2"

	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
)

const version string = "0.0.1"
const dbFile string = "snippets.sqlite"

//go:embed empty.sqlite
var emptySqliteDb []byte

//go:generate cp -r ../../react-frontend/build ./frontend
//go:embed frontend
var frontendFs embed.FS

var db *sql.DB

type Snippet struct {
	Id          uuid.UUID `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Language    string    `json:"language"`
	Value       string    `json:"value"`
}

func main() {
	var err error

	versionPtr := flag.Bool("version", false, "show version and exit")
	portPtr := flag.String("port", "55555", "port the backend uses")
	devPtr := flag.Bool("dev", false, "running in development environment")
	flag.Parse()
	if *versionPtr {
		fmt.Println(version)
		return
	}

	isDev := *devPtr
	if isDev {
		fmt.Printf("isDev %v", isDev)
	}

	port := *portPtr

	// ensure snippets.sqlite exists (extract initialized empty one from binary if not ~16KB)
	if _, err = os.Stat(dbFile); errors.Is(err, os.ErrNotExist) {
		// snippets.sqlite does not exist -> write empty one
		err = os.WriteFile(dbFile, emptySqliteDb, 0600)
		if err != nil {
			log.Fatalf("database file '%s' wasn't found and couldn't be created due to error: %s", dbFile, err.Error())
			os.Exit(1)
		}
	}

	// setup sql conn
	db, err = openDb()
	if err != nil {
		log.Fatalf("error opening db %v", err)
	}
	defer db.Close()
	// end setup sql

	e := echo.New()

	e.HideBanner = true

	if !isDev {
		// serve SPA files from memory, coming from inside the executable embedded at build time
		e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
			Root:       ".",
			Filesystem: getFrontendFileSystem(),
		}))
	}

	e.GET("/api/snippet", getSnippetsHandler)
	e.GET("/api/snippet/:id", withParsedIdParam(getSnippetHandler))
	e.POST("/api/snippet", createSnippetHandler)
	e.PUT("/api/snippet/:id", withParsedIdParam(updateSnippetHandler))
	e.DELETE("/api/snippet/:id", withParsedIdParam(deleteSnippetHandler))

	e.GET("/api/setting", readSettings)
	e.POST("/api/setting", writeSettings)

	e.POST("/api/hideUI", hideUI)

	s := &http2.Server{
		MaxConcurrentStreams: 250,
		MaxReadFrameSize:     1048576,
		IdleTimeout:          10 * time.Second,
	}
	if err = e.StartH2CServer(":"+port, s); err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

func openDb() (*sql.DB, error) {
	// wd, _ := os.Getwd()
	// fmt.Printf("wd %q", wd)
	driver := "sqlite3"
	dataSourceName := "file:" + dbFile //?cache=shared"
	return sql.Open(driver, dataSourceName)
}

func hideUI(c echo.Context) error {
	log.Println("hideUI")
	var scriptPath string
	if exists("./toggle.py") {
		scriptPath = "./toggle.py"
	} else if exists("../../scripts/toggle.py") {
		scriptPath = "../../scripts/toggle.py"
	} else {
		return fmt.Errorf("toggle script not found")
	}

	var args []string
	pasteParam := c.QueryParam("paste")
	if pasteParam != "" {
		pasteArg := "-paste=" + pasteParam
		args = append(args, pasteArg)
	}

	log.Printf("running script: %s with args %v", scriptPath, args)

	cmd := exec.Command(scriptPath, args...)
	out, err := cmd.Output()

	if err != nil {
		exitErr := err.(*exec.ExitError)
		log.Printf("script ran with error: %v, stderr: %s\nstdout: %s", exitErr, string(exitErr.Stderr), string(out))
		return err
	}

	log.Printf("script ran successfully, stdout:\n%s", string(out))

	return nil
}

func exists(path string) bool {
	_, err := os.Stat(path)
	return err == nil || errors.Is(err, fs.ErrExist)
}

func getFrontendFileSystem() http.FileSystem {
	// if isDev {
	// 	log.Print("using dev mode")
	// 	return http.FS(os.DirFS("../../react-frontend/build/"))
	// }

	// log.Print("using embed mode")
	fsys, err := fs.Sub(frontendFs, "frontend")
	if err != nil {
		panic(err)
	}

	return http.FS(fsys)
}
