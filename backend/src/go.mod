module snippets

go 1.16

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/labstack/echo/v4 v4.6.1 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
	golang.org/x/net v0.0.0-20211118161319-6a13c67c3ce4 // indirect
)
