package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"

	"github.com/google/uuid"
)

func getSnippets(ctx context.Context) ([]Snippet, error) {
	rows, err := db.QueryContext(ctx, `
SELECT snippet_id, name, description, language, value FROM snippet
`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return rowsToSnippets(rows)
}

func searchSnippets(query string, ctx context.Context) ([]Snippet, error) {
	splitQuery := strings.Split(query, " ")

	// make interface arr to be able to pass as args to Statement::QueryContext
	// can't cast, see https://stackoverflow.com/questions/12990338/cannot-convert-string-to-interface
	sqlArgs := make([]interface{}, len(splitQuery))

	// lower query parts
	// add % for sql like query
	for i, v := range splitQuery {
		sqlArgs[i] = "%" + strings.ToLower(v) + "%"
	}

	// simple like search, maybe later use fulltext search FTS5, see https://www.sqlite.org/fts5.html#overview_of_fts5
	// where every part of the search query must occur at least once in the search fields.
	// The snippet value is excluded from the search.
	sqlWhere := ""
	lastPartIndex := len(splitQuery) - 1
	for i := range splitQuery {
		sqlParam := "$" + fmt.Sprint(i+1)
		sqlWhere +=
			"(name LIKE " + sqlParam +
				" OR LOWER(description) LIKE " + sqlParam +
				" OR LOWER(language) LIKE " + sqlParam +
				")"
		if i < lastPartIndex {
			sqlWhere += " AND "
		}
	}

	sql := "SELECT snippet_id, name, description, language, value FROM snippet WHERE " + sqlWhere
	// log.Printf("%s", sql)
	stmt, err := db.PrepareContext(ctx, sql)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, sqlArgs...)
	if err != nil {
		return nil, err
	}
	return rowsToSnippets(rows)
}

func getSnippetById(ctx context.Context, id uuid.UUID) (*Snippet, error) {
	stmt, err := db.PrepareContext(ctx, `
SELECT snippet_id, name, description, language, value FROM snippet
WHERE snippet_id = $1
	`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(ctx, id)
	return rowToSnippet(row)
}

func createSnippet(ctx context.Context, s *Snippet) (uuid.UUID, error) {
	stmt, err := db.PrepareContext(ctx, `
INSERT INTO snippet (snippet_id, name, description, language, value)
VALUES (:id, :name, :description, :language, :value)
	`)
	if err != nil {
		log.Printf("createUser prepareContext err: %v", err)
		return uuid.Nil, err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx,
		sql.Named("id", s.Id.String()),
		sql.Named("name", s.Name),
		sql.Named("description", s.Description),
		sql.Named("language", s.Language),
		sql.Named("value", s.Value))
	if err != nil {
		return uuid.Nil, err
	}

	return s.Id, nil
}

func updateSnippet(ctx context.Context, s *Snippet) error {
	stmt, err := db.PrepareContext(ctx, `
UPDATE snippet 
SET 
	name = :name, 
	description = :description, 
	language = :language, 
	value = :value
WHERE snippet_id = :id
	`)
	if err != nil {
		log.Printf("createUser prepareContext err: %v", err)
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx,
		sql.Named("id", s.Id.String()),
		sql.Named("name", s.Name),
		sql.Named("description", s.Description),
		sql.Named("language", s.Language),
		sql.Named("value", s.Value))
	if err != nil {
		return err
	}

	return nil
}

func deleteSnippet(ctx context.Context, id uuid.UUID) error {
	_, err := db.ExecContext(ctx, `
DELETE FROM snippet WHERE snippet_id = $1
	`, id.String())
	if err != nil {
		log.Printf("createUser prepareContext err: %v", err)
		return err
	}
	// TODO check res.RowsAffected()

	return nil
}

func rowToSnippet(row *sql.Row) (*Snippet, error) {
	var s Snippet
	if err := row.Scan(&s.Id, &s.Name, &s.Description, &s.Language, &s.Value); err != nil {
		return nil, err
	}

	return &s, nil
}

func rowsToSnippets(rows *sql.Rows) ([]Snippet, error) {
	result := make([]Snippet, 0)
	for rows.Next() {
		var s Snippet
		if err := rows.Scan(&s.Id, &s.Name, &s.Description, &s.Language, &s.Value); err != nil {
			return nil, err
		}
		result = append(result, s)
	}

	return result, nil
}
