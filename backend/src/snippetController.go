package main

import (
	"net/http"
	"strings"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

func getSnippetsHandler(c echo.Context) error {
	query := strings.Trim(c.QueryParam("q"), " ")
	isSearch := query != ""

	var ss []Snippet
	var err error
	if isSearch {
		ss, err = searchSnippets(query, c.Request().Context())
	} else {
		// get all
		ss, err = getSnippets(c.Request().Context())
	}
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, ss)
}

func getSnippetHandler(id uuid.UUID, c echo.Context) error {
	s, err := getSnippetById(c.Request().Context(), id)
	if err != nil {
		// TODO not found or internal server error
		return c.String(http.StatusNotFound, "err")
	}

	return c.JSON(http.StatusOK, s)
}

func createSnippetHandler(c echo.Context) error {
	s := new(Snippet)
	err := c.Bind(s)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	if s.Id == uuid.Nil {
		return c.String(http.StatusBadRequest, "all zero uuid not allowed")
	}

	fixUserInputs(s)

	_, err = createSnippet(c.Request().Context(), s)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, "")
}

func updateSnippetHandler(id uuid.UUID, c echo.Context) error {
	s := new(Snippet)
	err := c.Bind(s)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	if s.Id == uuid.Nil {
		return c.String(http.StatusBadRequest, "all zero uuid not allowed")
	}

	if id != s.Id {
		return c.String(http.StatusBadRequest, "path id and payload id doesn't match")
	}

	fixUserInputs(s)

	err = updateSnippet(c.Request().Context(), s)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, "")
}

func deleteSnippetHandler(id uuid.UUID, c echo.Context) error {
	err := deleteSnippet(c.Request().Context(), id)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, nil)
}

type HandlerFuncTakingId func(id uuid.UUID, c echo.Context) error

func withParsedIdParam(idHandler HandlerFuncTakingId) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, err := uuid.Parse(c.Param("id"))
		if err != nil {
			return c.String(http.StatusBadRequest, "id is not uuid")
		}

		return idHandler(id, c)
	}
}

func fixUserInputs(s *Snippet) {
	s.Name = strings.TrimSpace(s.Name)
	s.Language = strings.TrimSpace(s.Language)
	s.Description = strings.TrimSpace(s.Description)
	// not trimming value because this could have intentional whitespace
}
