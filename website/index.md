---
title: Tetsu Snippet Manager
lang: en
---

# Characteristics
- offline & private
    - data stored in local sqlite database file
        - sync/backup yourself with whatever you want, it's just a file
    - no telemetry
- lightweight
    - ~ 20 MB disk space
    - low memory/cpu usage
    - the frontend runs in your installed browser

# Features
- instant search
- keyboard accessible with configurable shortcuts
- desktop integration: script to toggle frontend & paste to last active window (set global system shortcut)
    - currently only for Linux with X / X11 / Xorg (not Wayland)
- shift-paste support for pasting into Terminals
- syntax highlighting

# Screenshots
## Search
![Search](img/snippets_search_short.webp "Search")

## Create Snippet
![Create](img/snippets_create.webp "Create Snippet")

## Shortcuts
![Shortcuts](img/snippets_shortcuts.webp "Shortcuts")

# Download

## Linux
- [download latest release (alpha v0.0.1)](https://snippets.tetsu.eu/downloads/snippets-latest-linux-x64.zip) ~10 MB
- extract release file anywhere
- run install.py in a terminal (inspect before) to install as a systemd user service
- set custom system shortcut to execute the toggle.py script (X/X11 systems)
    - [KDE HowTo](https://userbase.kde.org/Tutorials/hotkeys)
    - [Gnome HowTo](https://help.gnome.org/users/gnome-help/stable/keyboard-shortcuts-set.html.en)
    - [Ubuntu HowTo](https://help.ubuntu.com/stable/ubuntu-help/keyboard-shortcuts-set.html.en)
- If you're using Firefox, you have to set about:config setting dom.events.testing.asyncClipboard to true so copying to clipboard works.

# Video
[Watch (~750KB)](video/snippetdemo.mp4)

# Misc
## Known Issues
- Firefox: needs about:config setting dom.events.testing.asyncClipboard set to true so copying to clipboard works

## Troubleshooting
- some browser extensions may interfere with shortcuts, such as vimium
- some shortcuts can't be configured because the browser prevents it (e.g. ctrl + n in chrome)

## Used Open Source Libraries and their Licenses
- Bootstrap 5: MIT, [https://getbootstrap.com](https://getbootstrap.com), [License](https://github.com/twbs/bootstrap/blob/main/LICENSE)
- React: MIT, [https://reactjs.org](https://reactjs.org), [License](https://github.com/facebook/react/blob/main/LICENSE)
- Echo: MIT, [https://echo.labstack.com](https://echo.labstack.com), [License](https://github.com/labstack/echo/blob/master/LICENSE)
- HighlightJS: BSD 3-Clause License, [https://highlightjs.org](https://highlightjs.org), [License](https://github.com/highlightjs/highlight.js/blob/main/LICENSE)
