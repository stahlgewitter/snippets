#!/usr/bin/env bash
pandoc -f commonmark_x --template pandoc-template.html5 --toc --toc-depth 1 --css css/style.css -o index.html index.md
